/** Do not rename this file **/
import React from 'react';

const defaultItems = [{ text: 'Add more things to do!', checked: false }];

export default class TodoWidget extends React.Component {
  static propTypes = {
    settings: React.PropTypes.object.isRequired,
  };

  static id = 'todo-widget';
  static widgetName = 'Todo List';
  static sizes = [[2, 2]];

  changeHandler(itemID) {
    const items = this.props.settings.get('items', defaultItems);
    items[itemID].checked = !items[itemID].checked;
    this.props.settings.set('items', items);
  }

  deleteHandler(itemID) {
    const items = this.props.settings.get('items', defaultItems).filter((item, id) => id !== itemID);
    this.props.settings.set('items', items);
  }

  keypressHandler(event) {
    if (event.key === 'Enter') {
      if (event.target.value !== '') {
        const items = this.props.settings.get('items', defaultItems);
        items.push({ text: event.target.value, checked: false });
        this.props.settings.set('items', items);

        event.target.value = '';
      }
    }
  }

  render() {
    return (
      <div className="container uk-flex uk-flex-column uk-form">
        <h2 className="uk-flex-item-none uk-container-center uk-margin-small-top uk-margin-small-bottom">Todo List</h2>
        <ul className="uk-list uk-list-striped list uk-flex-item-1 uk-margin-bottom-remove uk-margin-top-remove">
          {
            this.props.settings.get('items', defaultItems).map((data, id) =>
              <li key={id} className="item">
                <label className="uk-margin-small-left">
                  <input type="checkbox" checked={data.checked} onChange={this.changeHandler.bind(this, id)} />
                  <span className="uk-margin-small-left">{data.checked ? <strike>{data.text}</strike> : data.text}</span>
                </label>
                <i className="uk-close uk-float-right close-button" onClick={this.deleteHandler.bind(this, id)}></i>
              </li>
            )
          }
        </ul>
        <div className="uk-flex-item-none uk-margin uk-form add-form">
          <input type="text" onKeyPress={this.keypressHandler.bind(this)} placeholder="Press Enter to add new item" className="input-box" />
        </div>
      </div>
    );
  }
}
